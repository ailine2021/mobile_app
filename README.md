# Présentation du projet

Réalisation de la partie front d'une application mobile sécurisée avec à l'aide d'une Api :

* Une partie Publique : 
    * Une page de login
    * Une page de register
    * Une page d'oublie de mot de passe
    * Une page de ré-initialisation de mot de passe
* Une partie privée :
    * Page home
    * Page profile
    * Page Search


## Contenu du projet
* Liste de tous les utilisateurs de l'application 
* Informations sur les utilisateurs de l'application :
    * Prénom
    * Nom
    * Username
    * Email
    * Password
* Recherche d'un utilisateur par son email

## Fonctionnalités

* Gestion des utilisateurs:
    * Inscription 
    * Connexion
    * Oubli de mot de passe
    * Réinitialisation de mot de passe
    * Liste tous les users de l'application
    * Liste toutes les informations de l'utilisateur
    * Recherche d'un utilisateur à l'aide de son email

## Création des Wireframe et maquettes
* Lien Figma : https://www.figma.com/file/dijEchfrW6XfBsmNC4Kpp7/App-Mobile?node-id=0%3A1



